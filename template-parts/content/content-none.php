<section>
	<header>
		<?php if ( is_search() ) : ?>
			<h1>
				<?php
				printf(
					/* translators: %s: Search term. */
					esc_html__( 'Results for "%s"', 'clarcked-twentyone' ),
					'<span class="page-description search-term">' . esc_html( get_search_query() ) . '</span>'
				);
				?>
			</h1>
		<?php else : ?>
			<h1><?php esc_html_e( 'Nothing here', 'clarcked-twentyone' ); ?></h1>
		<?php endif; ?>
	</header>
	<div>
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>
			<?php
			printf(
				'<p>' . wp_kses(
					__( 'Ready to publish your first post? <a href="%s">Get started here</a>.', 'clarcked-twentyone' ),
					array(
						'a' => array(
							'href' => array(),
						),
					)
				) . '</p>',
				esc_url( admin_url( 'post-new.php' ) )
			);
			?>
		<?php elseif ( is_search() ) : ?>
			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'clarcked-twentyone' ); ?></p>
			<?php //get_search_form(); ?>
		<?php else : ?>
			<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'clarcked-twentyone' ); ?></p>
			<?php //get_search_form(); ?>
		<?php endif; ?>
	</div>
</section>
