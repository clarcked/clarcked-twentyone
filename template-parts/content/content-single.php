<article id="post-<?php the_ID(); ?>" <?php post_class("post-single"); ?>>
	<header>
		<?php the_title( '<h1>', '</h1>' ); ?> 
	</header>

	<div class="post-content">
        <?php if ( has_post_thumbnail() ) : ?>
        <a class="thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
            <?php the_post_thumbnail(); ?>
        </a>
        <?php endif; ?>
		<?php
		the_content();
		wp_link_pages(
			array(
				'before'   => '<nav class="page-links" aria-label="' . esc_attr__( 'Page', 'clarcked-twentyone' ) . '">',
				'after'    => '</nav>', 
				'pagelink' => esc_html__( 'Page %', 'clarcked-twentyone' ),
			)
		);
		?> 
	</div> 
	<footer>
		<?php //meta_footer(); ?>
	</footer> 
	<?php if ( ! is_singular( 'attachment' ) ) : ?>
		<?php get_template_part( 'template-parts/post/author-bio' ); ?>
	<?php endif; ?>
</article>
