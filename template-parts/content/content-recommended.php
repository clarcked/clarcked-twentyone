<article id="post-<?php the_ID(); ?>" <?php post_class("post post-recommended"); ?> >
	<a href="<?php echo the_permalink(); ?>"><?php the_title('<div class="h1">', '</div>'); ?></a>
	<section class="post-content">
		<div>
			<?php get_template_part( 'template-parts/excerpt/excerpt', get_post_format() ); ?> 
		</div>
		<?php if ( has_post_thumbnail() ) : ?>
		    <a class="thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
		        <?php the_post_thumbnail(array(100,100)); ?>
		    </a>
		<?php endif; ?>
	</section>
	<footer>
		<ul>
			<li><a href="<?php the_permalink(); ?>"><?php the_date("M d"); ?></a></li>
			<li><a href="<?php the_permalink(); ?>">
				<?php echo read_time(apply_filters( 'the_content', get_the_content() )) ?>		
			</a></li>
		</ul>
	</footer>
</article>