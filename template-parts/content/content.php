<article id="post-<?php the_ID(); ?>" <?php post_class("post post-excerpt"); ?> >
    <header>
        <a href="<?php get_the_author_link() ?>">
            <?php echo get_avatar(get_the_author_meta('ID'), 32); ?>
        </a>
        <a class="author-name" href="<?php get_the_author_link() ?>"><?php the_author() ?></a>
        <?php $categories = get_the_category();
            if ( ! empty( $categories ) ) { ?>
                <span><?php echo __("in") ?></span>
                <?php $categories = get_the_category();
                    if ( ! empty( $categories ) ) { ?>
                        <span><?php echo __("in") ?></span>
                        <div class="post-categories">
                            <?php the_category('&bull;'); ?>
                        </div>
                <?php  } ?>
         <?php  } ?>
    </header>
    <a href="<?php echo the_permalink(); ?>"><h1><?php the_title(); ?></h1></a>
    <section class="post-content">
        <div>
            <?php get_template_part('template-parts/excerpt/excerpt', get_post_format()); ?>
        </div>
        <?php if (has_post_thumbnail()) : ?>
            <a class="thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <?php the_post_thumbnail(array(100, 100)); ?>
            </a>
        <?php endif; ?>
    </section>
    <footer>
        <ul>
            <li><a href="<?php the_permalink(); ?>"><?php the_date("M d"); ?></a></li>
            <li>
                <a href="<?php the_permalink(); ?>">
                    <?php echo read_time(apply_filters('the_content', get_the_content())) ?>
                </a>
            </li>
            <?php the_tags("<li>", "</li><li>", "</li>"); ?>
        </ul>
    </footer>
</article>

