			</main>
			<aside id="side-left" class="side">
				<form class="form search-form" action="<?php echo home_url( '/' ); ?>">
					<div class="field">
						<input type="text" name="s" />
						<div class="icon">
							<i class="ico-search"></i>
						</div>
					</div>
				</form>
				<div class="widget widget-posts-recommended">
					<div class="widget-title">
						<?php echo __("Recommended for you", "clarcked-twentyone") ?>
					</div>
					<div class="widget-content">
						<?php
							while ( have_posts() ) {
								the_post();
								get_template_part( 'template-parts/content/content-recommended', get_post_format() );
							} 
						?>
					</div>
				</div>
			</aside>
		</div>
	</div>
<?php wp_footer(); ?>
</body>
</html>