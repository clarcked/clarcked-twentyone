<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php wp_head(); ?>
	<script data-ad-client="ca-pub-3192455108049852" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-205582172-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-205582172-2');
    </script>

</head>
<body <?php body_class();?>>
<?php wp_body_open(); ?>
	<div id="page" class="site">
		<nav id="header" class="site-header">
			<ul>
				<li>
					<div id="brand">
						<?php if ( has_custom_logo() ) : ?>
							<div class="site-logo"><?php the_custom_logo(); ?></div>
						<?php endif ?>
						<div class="brand-desc">
							<div class="site-name"><?php bloginfo( 'name' ); ?></div>
							<div class="site-desc"><?php bloginfo( 'description' ); ?></div>
						</div> 
					</div>
				</li>
			</ul>
		</nav>
		<div id="content" class="site-content">
			<main id="main" class="site-main">