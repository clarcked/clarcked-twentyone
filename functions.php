<?php

if(! function_exists("read_time")){
    function read_time($content){
        $read_time = str_word_count($content, 0) / 250;
        return sprintf(__("%d min read", "clarcked-twentyone"), $read_time);
    }
}

if (! function_exists('clarcked_twentyone_setup')){
    function clarcked_twentyone_setup(){
        load_theme_textdomain( 'clarcked-twentyone', get_template_directory() . '/languages' );
        add_theme_support( 'title-tag' );
        add_theme_support(
            'post-formats',
            array(
                'link',
                'aside',
                'gallery',
                'image',
                'quote',
                'status',
                'video',
                'audio',
                'chat',
            )
        );
        add_theme_support(
            'html5',
            array(
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
                'style',
                'script',
                'navigation-widgets',
            )
        );
        add_theme_support( 'post-thumbnails' );
        $logo_width  = 150;
        $logo_height = 150;
        add_theme_support(
            'custom-logo',
            array(
                'height'               => $logo_height,
                'width'                => $logo_width,
                'flex-width'           => true,
                'flex-height'          => true,
                'unlink-homepage-logo' => true,
            )
        );
    }
}

add_action('after_setup_theme', 'clarcked_twentyone_setup'); 


function clarcked_twentyone_scripts() {
    wp_enqueue_style( 'clarcked-twentyone-style', get_template_directory_uri() . '/style.css', array(), wp_get_theme()->get( 'Version' ) );
}
add_action( 'wp_enqueue_scripts', 'clarcked_twentyone_scripts' );
 

function et_excerpt_length($length) {
    return 35;
}

add_filter('excerpt_length', 'et_excerpt_length');
  
function clarcked_twentyone_excerpt_more( $more ) {
    return sprintf( '<a href="%1$s" class="more-link"><i class="icon ico-more-horizontal"></i><span>%2$s</span></a>',
          esc_url( get_permalink( get_the_ID() ) ),
          sprintf( __( 'Continue reading %s', 'clarcked-twentyone' ), '<span class="screen-reader-text">' . get_the_title( get_the_ID() ) . '</span>' )
    );
}

add_filter( 'excerpt_more', 'clarcked_twentyone_excerpt_more' );

// https://gist.github.com/clarcked/26e8d74fe3afb1106556aa0d5a98a581.js

function gist_func( $atts ){
	$args = shortcode_atts( array(
		'src' => 'something',
	), $atts ); 
	return "<br/><code><script src='".$args["src"]."'></script></code>";
}
add_shortcode( 'gist', 'gist_func' );

