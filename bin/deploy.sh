#!/bin/bash

APPNAME=clarcked
PROJECT=Blog
AWS_PROFILE=default
STACK=$APPNAME-wordpress-blog
# STACK=$APPNAME-wordpress-security
TEMPLATE=infra

sam validate --profile $AWS_PROFILE --template "$(pwd)/aws/$TEMPLATE.yaml" && \
sam build --profile $AWS_PROFILE --template "$(pwd)/aws/$TEMPLATE.yaml" && \
sam deploy --profile $AWS_PROFILE --stack-name $STACK --parameter-overrides ApplicationName=$APPNAME \
	 --capabilities CAPABILITY_NAMED_IAM --template "$(pwd)/.aws-sam/build/template.yaml" --tags project=$PROJECT
	 
