#!/bin/bash
echo "watching files ..."
#CMD='echo "files changed ..." && cd ../blogproj && scripts/link.sh && cd ../twentyone'
CMD='echo "files changed ..." && cd ../blogproj && ./scripts/link.sh'
watchmedo shell-command \
    --patterns="*.css;*.php" \
    --recursive \
    --command="$CMD"