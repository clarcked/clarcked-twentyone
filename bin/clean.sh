#!/bin/bash

APPNAME="clarcked-twentyone"
TIME=$(date +%s)
THEME_PATH="/opt/bitnami/wordpress/wp-content/themes"

if [[ -d "$THEME_PATH/$APPNAME" ]]; then
	zip "$THEME_PATH/$APPNAME-$TIME.zip" "$THEME_PATH/$APPNAME"
	rm -r "$THEME_PATH/$APPNAME"	
fi
mkdir "$THEME_PATH/$APPNAME"
chmod -R 777 "$THEME_PATH/$APPNAME"