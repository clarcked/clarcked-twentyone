#!/bin/bash

APPNAME="clarcked-twentyone"
TIME=$(date +%s)
THEME_PATH="/opt/bitnami/wordpress/wp-content/themes"

if [[ -d "$THEME_PATH/$APPNAME" ]]; then
	chmod -R 777 "$THEME_PATH/$APPNAME"	
fi

/opt/bitnami/ctlscript.sh restart apache