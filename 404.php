<?php
get_header();
?>
	<header>
		<h1><?php esc_html_e( 'Nothing found here', 'clarcked-twentyone' ); ?></h1>
	</header>
	<div class="error-404 not-found default-max-width">
		<div class="page-content">
			<p>
				<?php esc_html_e( 'It looks like nothing was found at this location. Maybe try a search?', 'clarcked-twentyone' ); ?>
			</p>  
		</div> 
	</div>
<?php
get_footer();
